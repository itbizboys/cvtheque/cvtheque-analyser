FROM golang:1.15

RUN apt-get update && apt-get install -y curl bash poppler-utils wv unrtf tidy git \
  && curl -SL https://github.com/odise/go-cron/releases/download/v0.0.7/go-cron-linux.gz \
    | zcat > /usr/local/bin/go-cron \
  && chmod u+x /usr/local/bin/go-cron \
  && rm -rf /var/cache/apk/*

RUN mkdir -p /go/src/app
WORKDIR /go/src/app
RUN mkdir static

COPY . /go/src/app
RUN go get -d -v ./...
RUN go build

ENV SCHEDULE ${SCHEDULE:-* * * * * *}

COPY run.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/run.sh

EXPOSE 18080

ENTRYPOINT ["run.sh"]
CMD ["run.sh"]