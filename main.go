package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"unicode"

	"code.sajari.com/docconv"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"golang.org/x/text/transform"
	"golang.org/x/text/unicode/norm"
)

func main() {
	TreatUntreated()
}

type Cities struct {
	Cities []string `json:"cities"`
}

type Profiles struct {
	Profiles []string `json:"profiles"`
}

// func main() {
// 	// fmt.Println(strconv.FormatInt(unTreatedCounter(), 10))
// 	unTreatedFiles()

// }

func getCity() []string {
	// Open our jsonFile
	jsonFile, err := os.Open("schedulers/cities.json")
	// if we os.Open returns an error then handle it
	if err != nil {
		fmt.Println(err)
	}

	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonFile.Close()

	// read our opened xmlFile as a byte array.
	byteValue, _ := ioutil.ReadAll(jsonFile)

	// we initialize our Users array
	var cities Cities

	// we unmarshal our byteArray which contains our
	// jsonFile's content into 'users' which we defined above
	json.Unmarshal(byteValue, &cities)

	return cities.Cities
}

func getProfile() []string {
	// Open our jsonFile
	jsonFile, err := os.Open("schedulers/profile.json")
	// if we os.Open returns an error then handle it
	if err != nil {
		fmt.Println(err)
	}

	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonFile.Close()

	// read our opened xmlFile as a byte array.
	byteValue, _ := ioutil.ReadAll(jsonFile)

	// we initialize our Users array
	var profiles Profiles

	// we unmarshal our byteArray which contains our
	// jsonFile's content into 'users' which we defined above
	json.Unmarshal(byteValue, &profiles)

	return profiles.Profiles
}

func unTreatedCounter() int64 {
	clientOptions := options.Client().ApplyURI("mongodb://root:Azer1234@cvtheque-db:27017/?retryWrites=true&w=majority")
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		fmt.Println(err)
	}

	err = client.Ping(context.TODO(), nil)

	if err != nil {
		fmt.Println(err)
	}

	log.Println("Connected to MongoDB!")

	defer client.Disconnect(context.TODO())

	collection := client.Database("cvtheque-db").Collection("profile")
	itemCount, err := collection.CountDocuments(context.TODO(), bson.M{"isTreated": false})

	return itemCount
}

func isMn(r rune) bool {
	return unicode.Is(unicode.Mn, r) // Mn: nonspacing marks
}

// TreatUntreated gets untreated files analyses them to update the database
func TreatUntreated() {
	clientOptions := options.Client().ApplyURI("mongodb://root:Azer1234@cvtheque-db:27017/?retryWrites=true&w=majority")
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		fmt.Println(err)
	}

	err = client.Ping(context.TODO(), nil)

	if err != nil {
		fmt.Println(err)
	}

	log.Println("Connected to MongoDB!")

	defer client.Disconnect(context.TODO())

	collection := client.Database("cvtheque-db").Collection("profile")

	filterCursor, err := collection.Find(context.TODO(), bson.M{"isTreated": false})
	if err != nil {
		fmt.Println(err)
	}
	var unTreatedProfiles []bson.M
	if err = filterCursor.All(context.TODO(), &unTreatedProfiles); err != nil {
		fmt.Println(err)
	}

	var unTreatedP []string

	for i := 0; i < len(unTreatedProfiles); i++ {
		unTreatedP = append(unTreatedP, unTreatedProfiles[i]["filename"].(string))
	}

	itemCount, err := collection.CountDocuments(context.TODO(), bson.M{"isTreated": false})
	if err != nil {
		fmt.Println(err)
	}

	if itemCount >= 1 {
		for i := 0; i < len(unTreatedP); i++ {
			res, err := docconv.ConvertPath("static/" + unTreatedP[i])
			if err != nil {
				fmt.Println(err)
			}

			t := transform.Chain(norm.NFD, transform.RemoveFunc(isMn), norm.NFC)
			reString := strings.ReplaceAll(res.Body, "\n", " ")
			reString, _, _ = transform.String(t, reString)

			var city string
			cityList := getCity()
			for i := 0; i < len(cityList); i++ {
				if strings.Contains(strings.ToLower(reString), cityList[i]) {
					city = cityList[i]
					break
				}
			}

			var profile string
			profileList := getProfile()
			for i := 0; i < len(profileList); i++ {
				if strings.Contains(strings.ToLower(reString), profileList[i]) {
					profile = profileList[i]
					break
				}
			}
			result, err := collection.UpdateOne(
				context.TODO(),
				bson.M{"filename": unTreatedP[i]},
				bson.D{
					{"$set", bson.D{
						{"fileraw", reString},
						{"city", city},
						{"profile", profile},
						{"isTreated", true},
					}},
				},
			)
			if err != nil {
				fmt.Println(err)
			}
			fmt.Println("Updated %v Documents! "+unTreatedP[i], result.ModifiedCount)
		}
	}

}
